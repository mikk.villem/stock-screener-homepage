const path = require(`path`);
// Log out information after a build is done
exports.onPostBuild = ({ reporter }) => {
  reporter.info(`Your Gatsby site has been built!`);
};
// Create blog pages dynamically
exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions;

  // Create screener result pages
  const resultPostTemplate = path.resolve(`src/templates/dailyResultsPost.js`);
  const result = await graphql(`
    query Pages {
      fauna {
        allScreensSortedByDateDesc(_size: 10) {
          data {
            resultCount
            date
            _id
          }
        }
      }
    }
  `);
  result.data.fauna.allScreensSortedByDateDesc.data.forEach((result) => {
    createPage({
      path: `${result._id}`,
      component: resultPostTemplate,
      context: {
        id: result._id,
      },
    });
  });

  // Create watchlist page
};
