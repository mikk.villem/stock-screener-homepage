import React from "react";
import { graphql } from "gatsby";
import Layout from "../components/Layout";
import ResultCard from "../components/ResultCard";
import useDynamicRefs from "../components/hooks/useDynamicRefs";
import SearchBar from "../components/SearchBar";

const DailyResultsPost = ({ location, data }) => {
  const { results, date, resultCount } = data.fauna.findDailyScreenByID;
  const [getRef, setRef] = useDynamicRefs();

  const handleSearch = (s) => {
    getRef(s.trim().toUpperCase()).current.scrollIntoView();
  };

  return (
    <Layout location={location}>
      <div className="result-header">
        <h1 className="page-title">
          {date} | {resultCount} results
        </h1>

        <SearchBar onSearch={(s) => handleSearch(s)} />
      </div>

      <div className="image-grid">
        {results.map((result, i) => (
          <ResultCard
            ticker={result}
            key={i}
            date={date}
            childRef={setRef(result)}
          />
        ))}
      </div>
    </Layout>
  );
};

export default DailyResultsPost;

export const query = graphql`
  query ($id: ID!) {
    fauna {
      findDailyScreenByID(id: $id) {
        date
        resultCount
        results
      }
    }
  }
`;
