import * as React from "react";
import { graphql } from "gatsby";
import Layout from "../components/Layout";
import ResultCard from "../components/ResultCard";
import useDynamicRefs from "../components/hooks/useDynamicRefs";
import SearchBar from "../components/SearchBar";

const WatchlistPage = ({ data, location }) => {
  const styles = {
    imageGrid: {
      display: "flex",
      justifyContent: "center",
      flexWrap: "wrap",
      maxWidth: "1920px",
      gap: "30px",
    },
  };
  const watchlist = data.fauna.allInWatchlist.data || [];
  const [getRef, setRef] = useDynamicRefs();

  const handleSearch = (s) => {
    let instance = getRef(s.trim().toUpperCase()).current;
    instance.scrollIntoView();
  };
  return (
    <Layout location={location}>
      <div className="result-header">
        <h1 className="page-title">Watchlist | {watchlist.length}</h1>
        <SearchBar onSearch={(s) => handleSearch(s)} />
      </div>
      <div style={styles.imageGrid}>
        {watchlist.map((result, i) => (
          <ResultCard
            ticker={result.ticker}
            key={i}
            date={result.dateAdded}
            childRef={setRef(result.ticker)}
          />
        ))}
      </div>
    </Layout>
  );
};

export default WatchlistPage;

export const query = graphql`
  query {
    fauna {
      allInWatchlist {
        data {
          dateAdded
          ticker
        }
      }
    }
  }
`;
