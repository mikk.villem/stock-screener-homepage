import React, { useState, useEffect } from "react";
import { graphql, Link } from "gatsby";
import Layout from "../components/Layout";
import Card from "../components/Card";
import ResultCard from "../components/ResultCard";
import { Line } from "react-chartjs-2";
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend,
} from "chart.js";

ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend
);

const IndexPage = ({ data, location }) => {
  const { allScreensSortedByDateDesc } = data.fauna;
  const currentResults = allScreensSortedByDateDesc.data[0];
  const prevResults = allScreensSortedByDateDesc.data[1];

  const trend = Math.round(
    ((currentResults.resultCount - prevResults.resultCount) /
      prevResults.resultCount) *
      100
  );

  let chartData = {
    labels: allScreensSortedByDateDesc.data
      .map((result) => result.date.slice(5).replace("-", "/"))
      .reverse(),
    datasets: [
      {
        label: "Daily results",
        backgroundColor: "rgb(255, 99, 132)",
        borderColor: "rgb(255, 99, 132)",
        data: allScreensSortedByDateDesc.data
          .map((result) => result.resultCount)
          .reverse(),
      },
    ],
  };

  let chartOptions = {
    responsive: true,
    layout: { padding: 24 },
    title: {
      display: true,
      fontSize: 25,
      text: "Daily Results",
    },
    legend: {
      display: false,
    },
  };
  const [watchlist, setWatchlist] = useState([]);
  useEffect(() => {
    fetch("/.netlify/functions/watchlist-view", {
      method: "GET",
    })
      .then((resp) => {
        return resp.json();
      })
      .then((data) => {
        let result = [];
        data.data.map((ref) => result.push(ref.data));
        return setWatchlist(result);
      });
  }, []);
  return (
    <Layout location={location}>
      <div className="content">
        <nav id="date-nav" className="aside-nav">
          <h2>Seeds by date</h2>
          <ul>
            {allScreensSortedByDateDesc.data.map((result, i) => (
              <li key={i}>
                <Link to={result._id}>{result.date}</Link>
              </li>
            ))}
          </ul>
        </nav>
        <main className="main">
          <section>
            <Line data={chartData} options={chartOptions} />
          </section>
          <section className="card-section">
            <Card
              title="Today's seeds"
              number={currentResults.resultCount}
              trend={trend}
              link={currentResults._id}
              buttonText="open"
            />
            <Card
              title="Watchlist"
              number={watchlist.length}
              link="watchlist"
              buttonText="open"
            />
            <Card></Card>
          </section>
        </main>
        <aside id="watclist-nav" className="aside-nav">
          <h2>Latest in watchlist</h2>
          <ul>
            {watchlist.length > 0 &&
              watchlist.slice(0, 3).map((result, i) => (
                <li key={i}>
                  <ResultCard ticker={result.ticker} />
                </li>
              ))}
          </ul>
        </aside>
      </div>
    </Layout>
  );
};

export default IndexPage;

export const query = graphql`
  query {
    fauna {
      allScreensSortedByDateDesc(_size: 10) {
        data {
          resultCount
          date
          _id
        }
      }
    }
  }
`;
