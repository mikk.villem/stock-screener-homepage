import faunadb from "faunadb"; /* Import faunaDB sdk */

/* configure faunaDB Client with our secret */
const q = faunadb.query;
const client = new faunadb.Client({
  secret: process.env.GATSBY_FAUNADB_ADMIN_KEY,
  domain: "db.eu.fauna.com",
});

/* export our lambda function as named "handler" export */
exports.handler = (event, context, callback) => {
  /* parse the string body into a useable JS object */

  console.log("Function `watchlist-view` invoked");

  /* construct the fauna query */

  return client
    .query(
      q.Map(
        q.Paginate(q.Match(q.Index("allInWatchlistByDateDesc"))),
        q.Lambda((x, ref) => q.Get(ref))
      )
    )

    .then((response) => {
      return callback(null, {
        statusCode: 200,
        body: JSON.stringify(response),
      });
    })
    .catch((error) => {
      console.log("error", error);
      /* Error! return the error with statusCode 400 */
      return callback(null, {
        statusCode: 400,
        body: JSON.stringify(error),
      });
    });
};
