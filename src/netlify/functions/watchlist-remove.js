import faunadb from "faunadb";

const q = faunadb.query;
const client = new faunadb.Client({
  secret: process.env.GATSBY_FAUNADB_ADMIN_KEY,
  domain: "db.eu.fauna.com",
});

exports.handler = (event, context, callback) => {
  const pathArray = event.path.split("/");
  const id = pathArray[pathArray.length - 1];
  console.log(`Function 'watchlist-remove' invoked. delete id: ${id}`);
  return client
    .query(q.Delete(q.Ref(q.Collection("Watchlist"), id)))
    .then((response) => {
      console.log("success", response);
      return callback(null, {
        statusCode: 200,
        body: JSON.stringify(response),
      });
    })
    .catch((error) => {
      console.log("error", error);
      return callback(null, {
        statusCode: 400,
        body: JSON.stringify(error),
      });
    });
};
