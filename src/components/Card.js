import * as React from "react";
import { Link } from "gatsby";

const Card = ({ title, number, trend, link, buttonText = "open" }) => {
  return (
    <div className="card-container">
      <h2 className="card-header">{title}</h2>
      <div className="card-body">
        <p className="">{number}</p>
        {trend && (
          <div
            id="trend"
            className={trend > 0 ? "flip-horizontal green" : "red"}>
            {trendline}

            <p
              style={{
                display: "inline-block",
                paddingLeft: "1rem",
                fontSize: "1.125rem",
              }}>
              {trend} %
            </p>
          </div>
        )}
      </div>
      <Link to={link ? link : "#"} className="card-button">
        {buttonText}
      </Link>
    </div>
  );
};

export default Card;

const trendline = (
  <svg
    width="23"
    height="12"
    viewBox="0 0 23 12"
    fill="none"
    xmlns="http://www.w3.org/2000/svg">
    <path
      d="M6 6.5L5.33104 7.24329L5.93675 7.78844L6.59295 7.30524L6 6.5ZM12.1111 2L12.8226 1.2973L12.2147 0.681833L11.5182 1.19476L12.1111 2ZM20.9938 12C21.5461 12.0034 21.9966 11.5585 22 11.0062L22.0559 2.00638C22.0593 1.45411 21.6144 1.00362 21.0621 1.00019C20.5098 0.996763 20.0593 1.44169 20.0559 1.99396L20.0062 9.99381L12.0064 9.94412C11.4541 9.94069 11.0036 10.3856 11.0002 10.9379C10.9968 11.4902 11.4417 11.9407 11.994 11.9441L20.9938 12ZM0.331035 2.74329L5.33104 7.24329L6.66896 5.75671L1.66896 1.25671L0.331035 2.74329ZM6.59295 7.30524L12.7041 2.80524L11.5182 1.19476L5.40705 5.69476L6.59295 7.30524ZM11.3996 2.7027L20.2885 11.7027L21.7115 10.2973L12.8226 1.2973L11.3996 2.7027Z"
      fill="black"
    />
  </svg>
);
