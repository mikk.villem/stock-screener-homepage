import React from "react";
import { Helmet } from "react-helmet";

const Head = ({ pageName, title, description, link }) => {
  return (
    <Helmet>
      <html lang="en" />
      <title>{pageName ? `${pageName} | ${title}` : `${title}`}</title>
      <meta name="description" content={description} />

      <meta name="theme-color" content="#fff" />
      <meta property="og:type" content="website" />
      <meta
        property="og:title"
        content={pageName ? `${pageName} | ${title}` : `${title}`}
      />
      <meta name="og:description" content={description} />
      <meta property="og:url" content={link} />
      <link rel="preconnect" href="https://fonts.googleapis.com" />
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
      <link
        href="https://fonts.googleapis.com/css2?family=Calistoga&family=Poly&display=swap"
        rel="stylesheet"
      />
    </Helmet>
  );
};

export default Head;
