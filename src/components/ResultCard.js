import React from "react";
import * as resultCardStyles from "../styles/resultCard.module.css";

const ResultCard = ({ ticker, date, childRef = null }) => {
  //useContext for date ??

  const addToWatchlist = (watchlistItem) => {
    return fetch("/.netlify/functions/watchlist-add", {
      body: JSON.stringify(watchlistItem),
      method: "POST",
    }).then((response) => {
      return response.json();
    });
  };

  const handleClick = (ticker, dateAdded) => {
    const watchlistItem = {
      ticker,
      dateAdded,
    };

    addToWatchlist(watchlistItem)
      .then((response) => {
        console.log("API response", response);
        // set app state
      })
      .catch((error) => {
        console.log("API error", error);
      });
  };

  return (
    <div id={ticker} ref={childRef} className={resultCardStyles.cardContainer}>
      <div className={resultCardStyles.buttonContainer}>
        <a
          href={`https://finviz.com/quote.ashx?t=${ticker}`}
          target="_blank"
          rel="noreferrer noopener">
          <button className={resultCardStyles.buttonPrimary}>
            Open finviz
          </button>
        </a>
        <a
          href={`https://finance.yahoo.com/quote/${ticker}/chart?p=${ticker}`}
          target="_blank"
          rel="noreferrer noopener">
          <button>Open YF</button>
        </a>
        {date && (
          <button
            onClick={() => handleClick(ticker, date)}
            style={{ marginLeft: "auto" }}>
            Add to Watchlist
          </button>
        )}
      </div>
      <img
        src={`https://charts2.finviz.com/chart.ashx?s=m&t=${ticker}`}
        alt={`${ticker} chart`}
      />
    </div>
  );
};

export default ResultCard;
