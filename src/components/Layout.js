import React from "react";
import useSiteMetadata from "./SiteMetadata";
import Head from "./Head";
import NavBar from "./NavBar";

const Layout = ({ children, pageName, location, metaDescription }) => {
  const seo = useSiteMetadata();
  const { title } = seo;
  const description = metaDescription || seo.description;

  return (
    <>
      <Head
        pageName={pageName}
        link={location?.href}
        {...{ title, description }}
      />

      <div className="wrap">
        <NavBar />
        {children}
      </div>
    </>
  );
};

export default Layout;
