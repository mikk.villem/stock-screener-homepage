import * as React from "react";
import { Link } from "gatsby";

const NavBar = () => {
  return (
    <header>
      <Link className="logo-wrap logo-text" to="/">
        <div className="icon-bg circle">
          <div className="icon"></div>
        </div>
        seeds
      </Link>
      <div></div>
    </header>
  );
};

export default NavBar;
