import * as React from "react";
import useFocus from "../components/hooks/useFocus";
import * as searchBarStyles from "../styles/searchBar.module.css";

const SearchBar = ({ onSearch }) => {
  const [search, setSearch] = React.useState("");
  const [searchRef, setSearchFocus] = useFocus();

  const handleSubmit = (e) => {
    e.preventDefault();
    onSearch(search);
  };

  React.useEffect(() => {
    const handleKeyDown = (e) => {
      if (document.activeElement !== searchRef.current && e.keyCode === 111) {
        e.preventDefault();
        setSearchFocus();
      }
    };

    if (typeof window !== `undefined`) {
      window.addEventListener("keydown", handleKeyDown);

      return () => {
        window.removeEventListener("keydown", handleKeyDown);
      };
    }
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <form className={searchBarStyles.wrap}>
      <input
        ref={searchRef}
        type="search"
        value={search}
        onChange={(e) => setSearch(e.target.value)}
        placeholder='Search tickers (Press "/" to search)'
        className={searchBarStyles.input}
      />
      <button
        type="submit"
        className={searchBarStyles.submit}
        onClick={(e) => handleSubmit(e)}>
        <svg
          xmlns="http://www.w3.org/2000/svg"
          className={searchBarStyles.icon}
          fill="none"
          viewBox="0 0 24 24"
          stroke="currentColor"
          strokeWidth={2}>
          <path
            strokeLinecap="round"
            strokeLinejoin="round"
            d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"
          />
        </svg>
      </button>
    </form>
  );
};

export default SearchBar;
